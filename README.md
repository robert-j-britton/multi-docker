
## Running the project in Kubernetes

### Locally

add the cluster secret to store the postgres password.
```
kubectl create secret generic pgpassword --from-literal PGPASSWORD=<value>
```

apply configurations to minikube node from the project root.
```
kubectl apply -f kubernetes
```
