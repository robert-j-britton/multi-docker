docker build -t rbritton/multi-client:latest -t rbritton/multi-client:${CIRCLE_SHA1} -f ./client/Dockerfile ./client
docker build -t rbritton/multi-server:latest -t rbritton/multi-server:${CIRCLE_SHA1} -f ./server/Dockerfile ./server
docker build -t rbritton/multi-worker:latest -t rbritton/multi-worker:${CIRCLE_SHA1} -f ./worker/Dockerfile ./worker
docker push rbritton/multi-client:latest
docker push rbritton/multi-server:latest
docker push rbritton/multi-worker:latest
docker push rbritton/multi-client:${CIRCLE_SHA1}
docker push rbritton/multi-server:${CIRCLE_SHA1}
docker push rbritton/multi-worker:${CIRCLE_SHA1}
kubectl apply -f kubernetes
kubectl set image deployments/client-deployment client=rbritton/multi-client:${CIRCLE_SHA1}
kubectl set image deployments/worker-deployment worker=rbritton/multi-worker:${CIRCLE_SHA1}
kubectl set image deployments/server-deployment server=rbritton/multi-server:${CIRCLE_SHA1}
